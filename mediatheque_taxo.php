<?php

if( !defined ( 'mediatheque_chateau_du_loir_taxo')){
    function mediatheque_chateau_du_loir_taxo(){

        /**
         * Taxonomy : Genre
         */

        $labels = array(
            'name'              => __( 'Genres', 'enssop' ),
            'singular_name'     => __( 'Genre', 'enssop' ),
            'search_items'      =>  __( 'Rechercher un genre', 'enssop' ),
            'all_items'         => __( 'Tous les genres', 'enssop' ),
            'parent_item'       => __( 'Genre Parent', 'enssop' ),
            'parent_item_colon' => __( 'Genre Parent :', 'enssop' ),
            'edit_item'         => __( 'Modifier le genre', 'enssop' ),
            'update_item'       => __( 'Modifier le genre', 'enssop' ),
            'add_new_item'      => __( 'Ajouter un genre' , 'enssop'),
            'new_item_name'     => __( 'Nouveau genre', 'enssop' ),
            'menu_name'         => __( 'Genre' , 'enssop' ),
        );

        $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' =>  true,
            'query_var'         => true,
            'rewrite'           => array( 'slug'    =>  'genre'),
        );
        register_taxonomy( 'genre', array( 'book', 'film', 'cd', 'game' ), $args);

        /**
         * Taxonomy : Type d'évènements
         */

        $labels = array(
            'name'              => __( "Types d'évènements", 'enssop' ),
            'singular_name'     => __( "Type d'évènements", 'enssop' ),
            'search_items'      =>  __( "Rechercher un type d'évènement", 'enssop' ),
            'all_items'         => __( "Tous les types d'évènements", 'enssop' ),
            'parent_item'       => __( "Type d'évènements Parent", 'enssop' ),
            'parent_item_colon' => __( "Type d'évènements Parent :", 'enssop' ),
            'edit_item'         => __( "Modifier le type d'évènement", 'enssop' ),
            'update_item'       => __( "Modifier le type d'évènement", 'enssop' ),
            'add_new_item'      => __( "Ajouter un type d'évènement" , 'enssop'),
            'new_item_name'     => __( "Nouveau type d'évènement", 'enssop' ),
            'menu_name'         => __( "Type d'évènement" , 'enssop' ),
        );

        $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'public'            => true,
            'publicly_queryable'    => false,
            'show_ui'           => true,
            'show_in_menu'      => true,
            'show_in_nav_menus' => true,
            'show_in_quick_edit' => true,
            'show_admin_column' =>  true,
            'query_var'         => false,

        );
        register_taxonomy( 'event_type', array( 'event' ), $args);

        /**
         * Taxonomy: Qualificatif.
         */

        $labels = array(
            "name"          => __( "Qualificatifs", "ensssop" ),
            "singular_name" => __( "Qualificatif", "ensssop" ),
            "menu_name"     => __( "Qualificatif", "ensssop" ),
            "all_items"     => __( "Tous les Qualificatifs", "ensssop" ),
            "edit_item"     => __( "Modifier le qualificatif", "ensssop" ),
            "view_item"     => __( "Voir le Qualificatif", "ensssop" ),
        );

        $args = array(
            "label"                 => __( "Qualificatifs", "enssop" ),
            "labels"                => $labels,
            "public"                => true,
            "publicly_queryable"    => false,
            "hierarchical"          => false,
            "show_ui"               => true,
            "show_in_menu"          => true,
            "show_in_nav_menus"     => true,
            "query_var"             => false,
            "show_admin_column"     => true,
            "show_in_rest"          => true,
            "show_in_quick_edit"    => true,
        );
        register_taxonomy( "qualifier", array( 'book', 'film', 'cd', 'game', 'event' ), $args );

    }
}

add_action('init', 'mediatheque_chateau_du_loir_taxo');
