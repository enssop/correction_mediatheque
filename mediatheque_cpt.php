<?php

if(! defined('mediatheque_chateau_du_loir_cpt')){
    function mediatheque_chateau_du_loir_cpt(){


        /**
         * Custom Post Type : Livres
         */
        $labels = array(
            "name"                  => __( "Bibliothèque", "enssop" ),
            "singular_name"         => __( "Livre", "enssop" ),
            "menu_name"             => __( "Bibliothèque", "enssop" ),
            "all_items"             => __( "Tous les livres", "enssop" ),
            "add_new"               => __( "Ajouter un livre", "enssop" ),
            "add_new_item"          => __( "Ajouter un nouveau livre", "enssop" ),
            "edit_item"             => __( "Ajouter le livre", "enssop" ),
            "new_item"              => __( "Nouveau livre", "enssop" ),
            "view_item"             => __( "Voir le livre", "enssop" ),
            "view_items"            => __( "Voir les livres", "enssop" ),
            "search_items"          => __( "Chercher un livre", "enssop" ),
            "not_found"             => __( "Pas de livre trouvé", "enssop" ),
            "not_found_in_trash"    => __( "Pas de livre trouvé dans la corbeille", "enssop" ),
            "featured_image"        => __( "Image mise en avant pour ce livre", "enssop" ),
            "set_featured_image"    => __( "Définir l'image mise en avant pour ce livre", "enssop" ),
            "remove_featured_image" => __( "Supprimer l'image mise en avant pour ce livre", "enssop" ),
            "use_featured_image"    => __( "Utiliser comme image mise en avant pour ce livre", "enssop" ),
            "archives"              => __( "Type de recette", "enssop" ),
            "insert_into_item"      => __( "Ajouter au livre", "enssop" ),
            "uploaded_to_this_item" => __( "Ajouter au livre", "enssop" ),
            "filter_items_list"     => __( "Filtrer la liste de livres", "enssop" ),
            "items_list_navigation" => __( "Naviguer dans la liste de livres", "enssop" ),
            "items_list"            => __( "Liste de livres", "enssop" ),
            "attributes"            => __( "Paramètres du livre", "enssop" ),
            "name_admin_bar"        => __( "Livre", "enssop" ),
        );

        $args= array(
            "label"                 => __('Bibliothèque', 'enssop'),
            "labels"                => $labels,
            "description"           =>  __('Référence les livres de la médiathèque', 'enssop'),
            "public"                => true,
            "publicly_queryable"    => true,
            "show_ui"               =>  true,
            "delete_with_user"      =>  false,
            "show_in_rest"          => false,
            "has_archive"           =>  true,
            "show_in_menu"          =>  true,
            "show_in_nav_menu"      => true,
            "menu_position"         =>  4,
            "exclude_from_search"   => false,
            "capability_type"       => "post",
            "map_meta_cap"          => true,
            "hierarchical"          => false,
            "rewrite"               => array( "slug" => "bibliotheque", "with_front" => true ),
            "query_var"             => 'livres',
            "menu_icon"             => "dashicons-book",
            "supports"              => array(      'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks',
                                                   'custom-fields', 'comments', 'revisions', 'page-attributes',
                                                   'post-formats' ),
        );

        register_post_type( 'book', $args );

        /**
         * Custom Post Type : Films
         */
        $labels = array(
            "name"                  => __( "Cinémathèque", "enssop" ),
            "singular_name"         => __( "Film", "enssop" ),
            "menu_name"             => __( "Cinémathèque", "enssop" ),
            "all_items"             => __( "Tous les films", "enssop" ),
            "add_new"               => __( "Ajouter un film", "enssop" ),
            "add_new_item"          => __( "Ajouter un nouveau film", "enssop" ),
            "edit_item"             => __( "Ajouter le film", "enssop" ),
            "new_item"              => __( "Nouveau film", "enssop" ),
            "view_item"             => __( "Voir le film", "enssop" ),
            "view_items"            => __( "Voir les films", "enssop" ),
            "search_items"          => __( "Chercher un film", "enssop" ),
            "not_found"             => __( "Pas de film trouvé", "enssop" ),
            "not_found_in_trash"    => __( "Pas de film trouvé dans la corbeille", "enssop" ),
            "featured_image"        => __( "Image mise en avant pour ce film", "enssop" ),
            "set_featured_image"    => __( "Définir l'image mise en avant pour ce film", "enssop" ),
            "remove_featured_image" => __( "Supprimer l'image mise en avant pour ce film", "enssop" ),
            "use_featured_image"    => __( "Utiliser comme image mise en avant pour ce film", "enssop" ),
            "archives"              => __( "Type de films", "enssop" ),
            "insert_into_item"      => __( "Ajouter au film", "enssop" ),
            "uploaded_to_this_item" => __( "Ajouter au film", "enssop" ),
            "filter_items_list"     => __( "Filtrer la liste de films", "enssop" ),
            "items_list_navigation" => __( "Naviguer dans la liste de films", "enssop" ),
            "items_list"            => __( "Liste de films", "enssop" ),
            "attributes"            => __( "Paramètres du film", "enssop" ),
            "name_admin_bar"        => __( "Film", "enssop" ),
        );

        $args= array(
            "label"                 => __('Cinémathèque', 'enssop'),
            "labels"                => $labels,
            "description"           =>  __('Référence les films de la médiathèque', 'enssop'),
            "public"                => true,
            "publicly_queryable"    => true,
            "show_ui"               =>  true,
            "delete_with_user"      =>  false,
            "show_in_rest"          => false,
            "has_archive"           =>  true,
            "show_in_menu"          =>  true,
            "show_in_nav_menu"      => true,
            "menu_position"         =>  4,
            "exclude_from_search"   => false,
            "capability_type"       => "post",
            "map_meta_cap"          => true,
            "hierarchical"          => false,
            "rewrite"               => array( "slug" => "cinematheque", "with_front" => true ),
            "query_var"             => 'films',
            "menu_icon"             => "dashicons-video-alt",
            "supports"              => array(      'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks',
                                                   'custom-fields', 'comments', 'revisions', 'page-attributes',
                                                   'post-formats' ),
        );

        register_post_type( 'film', $args );


        /**
         * Custom Post Type : Cds
         */
        $labels = array(
            "name"                  => __( "Médiathèque", "enssop" ),
            "singular_name"         => __( "CD", "enssop" ),
            "menu_name"             => __( "Médiathèque", "enssop" ),
            "all_items"             => __( "Tous les CDs", "enssop" ),
            "add_new"               => __( "Ajouter un CD", "enssop" ),
            "add_new_item"          => __( "Ajouter un nouveau CD", "enssop" ),
            "edit_item"             => __( "Ajouter le CD", "enssop" ),
            "new_item"              => __( "Nouveau CD", "enssop" ),
            "view_item"             => __( "Voir le CD", "enssop" ),
            "view_items"            => __( "Voir les CDs", "enssop" ),
            "search_items"          => __( "Chercher un CD", "enssop" ),
            "not_found"             => __( "Pas de CD trouvé", "enssop" ),
            "not_found_in_trash"    => __( "Pas de CD trouvé dans la corbeille", "enssop" ),
            "featured_image"        => __( "Image mise en avant pour ce CD", "enssop" ),
            "set_featured_image"    => __( "Définir l'image mise en avant pour ce CD", "enssop" ),
            "remove_featured_image" => __( "Supprimer l'image mise en avant pour ce CD", "enssop" ),
            "use_featured_image"    => __( "Utiliser comme image mise en avant pour ce CD", "enssop" ),
            "archives"              => __( "Type de CDs", "enssop" ),
            "insert_into_item"      => __( "Ajouter au CD", "enssop" ),
            "uploaded_to_this_item" => __( "Ajouter au CD", "enssop" ),
            "filter_items_list"     => __( "Filtrer la liste de CDs", "enssop" ),
            "items_list_navigation" => __( "Naviguer dans la liste de CDs", "enssop" ),
            "items_list"            => __( "Liste de CDs", "enssop" ),
            "attributes"            => __( "Paramètres du CD", "enssop" ),
            "name_admin_bar"        => __( "CD", "enssop" ),
        );

        $args= array(
            "label"                 => __('Médiathèque', 'enssop'),
            "labels"                => $labels,
            "description"           =>  __('Référence les CD de la médiathèque', 'enssop'),
            "public"                => true,
            "publicly_queryable"    => true,
            "show_ui"               =>  true,
            "delete_with_user"      =>  false,
            "show_in_rest"          => false,
            "has_archive"           =>  true,
            "show_in_menu"          =>  true,
            "show_in_nav_menu"      => true,
            "menu_position"         =>  4,
            "exclude_from_search"   => false,
            "capability_type"       => "post",
            "map_meta_cap"          => true,
            "hierarchical"          => false,
            "rewrite"               => array( "slug" => "mediatheque", "with_front" => true ),
            "query_var"             => 'cds',
            "menu_icon"             => "dashicons-album",
            "supports"              => array(      'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks',
                                                   'custom-fields', 'comments', 'revisions', 'page-attributes',
                                                   'post-formats' ),
        );

        register_post_type( 'cd', $args );

        /**
         * Custom Post Type : Jeux
         */
        $labels = array(
            "name"                  => __( "Ludothèque", "enssop" ),
            "singular_name"         => __( "Jeu", "enssop" ),
            "menu_name"             => __( "Ludothèque", "enssop" ),
            "all_items"             => __( "Tous les jeux", "enssop" ),
            "add_new"               => __( "Ajouter un jeu", "enssop" ),
            "add_new_item"          => __( "Ajouter un nouveau jeu", "enssop" ),
            "edit_item"             => __( "Ajouter le jeu", "enssop" ),
            "new_item"              => __( "Nouveau jeu", "enssop" ),
            "view_item"             => __( "Voir le jeu", "enssop" ),
            "view_items"            => __( "Voir les jeux", "enssop" ),
            "search_items"          => __( "Chercher un jeu", "enssop" ),
            "not_found"             => __( "Pas de jeu trouvé", "enssop" ),
            "not_found_in_trash"    => __( "Pas de jeu trouvé dans la corbeille", "enssop" ),
            "featured_image"        => __( "Image mise en avant pour ce jeu", "enssop" ),
            "set_featured_image"    => __( "Définir l'image mise en avant pour ce jeu", "enssop" ),
            "remove_featured_image" => __( "Supprimer l'image mise en avant pour ce jeu", "enssop" ),
            "use_featured_image"    => __( "Utiliser comme image mise en avant pour ce jeu", "enssop" ),
            "archives"              => __( "Type de jeux", "enssop" ),
            "insert_into_item"      => __( "Ajouter au jeu", "enssop" ),
            "uploaded_to_this_item" => __( "Ajouter au jeu", "enssop" ),
            "filter_items_list"     => __( "Filtrer la liste de jeux", "enssop" ),
            "items_list_navigation" => __( "Naviguer dans la liste de jeux", "enssop" ),
            "items_list"            => __( "Liste de jeux", "enssop" ),
            "attributes"            => __( "Paramètres du jeu", "enssop" ),
            "name_admin_bar"        => __( "Jeux", "enssop" ),
        );

        $args= array(
            "label"                 => __('Ludothèque', 'enssop'),
            "labels"                => $labels,
            "description"           =>  __('Référence les jeux de la médiathèque', 'enssop'),
            "public"                => true,
            "publicly_queryable"    => true,
            "show_ui"               =>  true,
            "delete_with_user"      =>  false,
            "show_in_rest"          => false,
            "has_archive"           =>  true,
            "show_in_menu"          =>  true,
            "show_in_nav_menu"      => true,
            "menu_position"         =>  4,
            "exclude_from_search"   => false,
            "capability_type"       => "post",
            "map_meta_cap"          => true,
            "hierarchical"          => false,
            "rewrite"               => array( "slug" => "mediatheque", "with_front" => true ),
            "query_var"             => 'jeux',
            "menu_icon"             => "dashicons-buddicons-activity",
            "supports"              => array(      'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks',
                                                   'custom-fields', 'comments', 'revisions', 'page-attributes',
                                                   'post-formats' ),
        );

        register_post_type( 'game', $args );

        /**
         * Custom Post Type : Evènements
         */
        $labels = array(
            "name"                  => __( "Evènements", "enssop" ),
            "singular_name"         => __( "Evènement", "enssop" ),
            "menu_name"             => __( "Evènements", "enssop" ),
            "all_items"             => __( "Tous les évènements", "enssop" ),
            "add_new"               => __( "Ajouter un évènement", "enssop" ),
            "add_new_item"          => __( "Ajouter un nouveau évènement", "enssop" ),
            "edit_item"             => __( "Ajouter l'évènement", "enssop" ),
            "new_item"              => __( "Nouvel évènement", "enssop" ),
            "view_item"             => __( "Voir l'évènement", "enssop" ),
            "view_items"            => __( "Voir les évènements", "enssop" ),
            "search_items"          => __( "Chercher un évènement", "enssop" ),
            "not_found"             => __( "Pas d'évènement trouvé", "enssop" ),
            "not_found_in_trash"    => __( "Pas d'évènement trouvé dans la corbeille", "enssop" ),
            "featured_image"        => __( "Image mise en avant pour cet évènement", "enssop" ),
            "set_featured_image"    => __( "Définir l'image mise en avant pour cet évènement", "enssop" ),
            "remove_featured_image" => __( "Supprimer l'image mise en avant pour cet évènement", "enssop" ),
            "use_featured_image"    => __( "Utiliser comme image mise en avant pour cet évènement", "enssop" ),
            "archives"              => __( "Type d'évènements", "enssop" ),
            "insert_into_item"      => __( "Ajouter à l'évènement", "enssop" ),
            "uploaded_to_this_item" => __( "Ajouter à l'évènement", "enssop" ),
            "filter_items_list"     => __( "Filtrer la liste d'évènements", "enssop" ),
            "items_list_navigation" => __( "Naviguer dans la liste d'évènements'", "enssop" ),
            "items_list"            => __( "Liste d'évènements", "enssop" ),
            "attributes"            => __( "Paramètres de l'évènement", "enssop" ),
            "name_admin_bar"        => __( "Evènements", "enssop" ),
        );

        $args= array(
            "label"                 => __('Evènements', 'enssop'),
            "labels"                => $labels,
            "description"           =>  __('Référence les évènements de la médiathèque', 'enssop'),
            "public"                => true,
            "publicly_queryable"    => true,
            "show_ui"               =>  true,
            "delete_with_user"      =>  false,
            "show_in_rest"          => false,
            "has_archive"           =>  true,
            "show_in_menu"          =>  true,
            "show_in_nav_menu"      => true,
            "menu_position"         =>  4,
            "exclude_from_search"   => false,
            "capability_type"       => "post",
            "map_meta_cap"          => true,
            "hierarchical"          => false,
            "rewrite"               => array( "slug" => "mediatheque", "with_front" => true ),
            "query_var"             => 'jeux',
            "menu_icon"             => "dashicons-megaphone",
            "supports"              => array(      'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks',
                                                   'custom-fields', 'comments', 'revisions', 'page-attributes',
                                                   'post-formats' ),
        );

        register_post_type( 'event', $args );
    }
}

add_action( 'init', 'mediatheque_chateau_du_loir_cpt');
