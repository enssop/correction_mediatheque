<?php

/**
 * Plugin Name:       Médiathèque Chateau du Loir
 * Plugin URI:        https://gitlab.com/enssop/correction_mediatheque
 * Description:       Correction du TP Massu consistant à créer des CPT pour la médiathèque de Chateau du Loir
 * Version:           1.0.0
 * Requires at least: 5.4
 * Requires PHP:      7.2
 * Author:            Christine Gunsenheimer <chrissienoodle>
 * License:           MIT
 * License URI:       https://mit-license.org/
 */

include 'mediatheque_cpt.php';
include 'mediatheque_taxo.php';